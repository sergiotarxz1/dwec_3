var respuesta;

function sumar_y_mas_cosas() {
	if (respuesta != null) {
		respuesta.remove();
		respuesta = null;
	}
	respuesta = document.createElement('p');
	let string = document.getElementById('array').value;
	let array = string.split("\n");
	let suma = 0;
	for ( element of array ) {
		if (!isNaN(element)) {
			let number = parseFloat(element);
			if (!isNaN(number)) {
				suma += number;
			}
		}
	}
	let numero_elementos = array.length
	respuesta.innerHTML = "El numero de elementos es " + numero_elementos + ", la suma de los númericos es " + suma + ".";
	document.body.appendChild(respuesta);
}
